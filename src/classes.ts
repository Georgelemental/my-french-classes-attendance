/*
my-french-classes-attendance attendance management software
Copyright (C) 2020  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

// Define class ClassOrProgram, used to store information about each class or program.
// "Class" is a reserved word in JS, hence the long name
export class ClassOrProgram {
	code: string;
	name: string;
	teacher: string;
	isClass: boolean;
	isMorning: boolean;
	supplies: Array<string>;
	students: Array<Student>;

	constructor(
		code: string,
		name: string,
		teacher: string,
		isClass: boolean,
		isMorning: boolean,
		supplies: Array<string>,
		students: Array<Student>
	) {
		this.code = code;
		this.name = name;
		this.teacher = teacher;
		this.isClass = isClass;
		this.isMorning = isMorning;
		this.supplies = supplies;
		this.students = students;
	}
}

export class Student {
	firstName: string;
	lastName: string;
	team: string;
	frenchClass: ClassOrProgram;
	program: ClassOrProgram;
	forms: string;
	afternoonClass: ClassOrProgram | null;

	constructor(
		firstName: string,
		lastName: string,
		team: string,
		frenchClass: ClassOrProgram,
		program: ClassOrProgram,
		forms: string
	) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.team = team;
		this.frenchClass = frenchClass;
		this.program = program;
		this.forms = forms;
		this.afternoonClass =
			frenchClass != null && program != null
				? frenchClass.isMorning
					? program
					: frenchClass
				: null;
	}
}
