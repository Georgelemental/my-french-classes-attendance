/*
my-french-classes-attendance attendance management software
Copyright (C) 2019  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

import { Student } from "./classes";

export class ColumnDescriptor {
	name: string;
	value: (student: Student) => string;
	width: number | null;
	attributes: object | null

	constructor(
		name: string,
		value: (student: Student) => string,
		width: number | null = null,
		attributes: object | null =  null
	) {
		this.name = name;
		this.value = value;
		this.width = width;
		this.attributes = attributes;
	}
}

// More variable definitions—specific to this function
const baseAttributes: any = {};
baseAttributes[DocumentApp.Attribute.HORIZONTAL_ALIGNMENT] =
	DocumentApp.HorizontalAlignment.CENTER;
baseAttributes[DocumentApp.Attribute.FONT_FAMILY] = "Times New Roman";
baseAttributes[DocumentApp.Attribute.FONT_SIZE] = 8;
baseAttributes[DocumentApp.Attribute.FOREGROUND_COLOR] = "#000000";
baseAttributes[DocumentApp.Attribute.LINE_SPACING] = 1;
baseAttributes[DocumentApp.Attribute.SPACING_BEFORE] = 0;
const titleAttributes: any = {};
Object.assign(titleAttributes, baseAttributes);
titleAttributes[DocumentApp.Attribute.BOLD] = true;
titleAttributes[DocumentApp.Attribute.SPACING_AFTER] = 0;
const subtitleAttributes: any = {};
Object.assign(subtitleAttributes, baseAttributes);
subtitleAttributes[DocumentApp.Attribute.BOLD] = true;
subtitleAttributes[DocumentApp.Attribute.SPACING_AFTER] = 5;
const tableAttributes: any = {};
Object.assign(tableAttributes, baseAttributes);
tableAttributes[DocumentApp.Attribute.BOLD] = false;
tableAttributes[DocumentApp.Attribute.SPACING_AFTER] = 0;
const tableHeaderAttributes: any = {};
Object.assign(tableHeaderAttributes, baseAttributes);
tableHeaderAttributes[DocumentApp.Attribute.BOLD] = true;
tableHeaderAttributes[DocumentApp.Attribute.SPACING_AFTER] = 0;

export function appendSheetToDoc(
	body: GoogleAppsScript.Document.Body,
	title: string,
	subtitle: string,
	students: Array<Student>,
	columnDescriptors: Array<ColumnDescriptor>
): void {
	let paragraphs = body.getParagraphs();
	const titleParagraph = paragraphs[paragraphs.length - 1];
	titleParagraph.appendText(title);
	titleParagraph.setHeading(DocumentApp.ParagraphHeading.TITLE);
	titleParagraph.setAttributes(titleAttributes);
	const subtitleParagraph = body.appendParagraph(subtitle);
	subtitleParagraph.setHeading(DocumentApp.ParagraphHeading.SUBTITLE);
	subtitleParagraph.setAttributes(subtitleAttributes);
	let tableArray: Array<Array<string>> = [];
	tableArray[0] = columnDescriptors.map(
		columnDescriptor => columnDescriptor.name
	);
	for (
		let studentIndex = 0, numStudents = students.length;
		studentIndex < numStudents;
		studentIndex++
	) {
		tableArray[studentIndex + 1] = columnDescriptors.map(columnDescriptor =>
			columnDescriptor.value(students[studentIndex])
		);
		//Logger.log(studentIndex);
	}
	let table = body.appendTable(tableArray);
	table.setAttributes(tableAttributes);
	//Logger.log(tableAttributes);
	const topRow = table.getRow(0);
	topRow.setAttributes(tableHeaderAttributes);
	for (
		let column = 0, numColumns = columnDescriptors.length;
		column < numColumns;
		column++
	) {
		const columnWidth = columnDescriptors[column].width;
		if (columnWidth !== null) {topRow.getCell(column).setWidth(columnWidth)};
		const attributes = columnDescriptors[column].attributes;
		if (attributes !== null) { 
			for (let row = 1; row < tableArray.length; row++) {
				table.getRow(row).getCell(column).setAttributes(attributes);
			}
		}

	}
	paragraphs = body.getParagraphs();
	const lastParagraph = paragraphs[paragraphs.length - 1];
	lastParagraph.appendPageBreak();
	const lastParagraphAttributes: any = {};
	lastParagraphAttributes[DocumentApp.Attribute.FONT_SIZE] = 1;
	lastParagraphAttributes[DocumentApp.Attribute.LINE_SPACING] = 1;
	lastParagraphAttributes[DocumentApp.Attribute.SPACING_BEFORE] = 0;
	lastParagraphAttributes[DocumentApp.Attribute.SPACING_AFTER] = 0;
	lastParagraph.setAttributes(lastParagraphAttributes);
}