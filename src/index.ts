/*
my-french-classes-attendance attendance management software
Copyright (C) 2020  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

export {
	fixFormSpreadsheets, updateAttendanceForms,
	updateAttendanceSheets,
	updateSuppliesSheets
} from "./attendanceUpdater";

export function onOpen() {
	// Add a custom menu to the spreadsheet.
	SpreadsheetApp.getUi() // Or DocumentApp, SlidesApp, or FormApp.
		.createMenu('Attendance')
		.addItem('Update attendance sheets', 'updateAttendanceSheets')
		.addItem('Update attendance forms', 'updateAttendanceForms')
		.addItem('Fix form spreadsheets', 'fixFormSpreadsheets')
		.addItem('Update supplies sheets', 'updateSuppliesSheets')
		.addToUi();
};