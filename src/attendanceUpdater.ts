/*
my-french-classes-attendance attendance management software
Copyright (C) 2020  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

import {
	findOrCreateFileFromMyFrenchClassesTemplate,
	getFileByName,
	moveFile
} from "./fileOps";
import { fullFrenchDate } from "./dateOps";
import { compareArrays } from "./arrayOps";
import { ClassOrProgram, Student } from "./classes";
import { classesAndPrograms } from "./studentInfo";
import { appendSheetToDoc, ColumnDescriptor } from "./createSheetDoc";

let nameColumnAttributes: any = {};
nameColumnAttributes[DocumentApp.Attribute.FONT_SIZE] = 10;
const emptyStringFunc = () => "";

const properties = PropertiesService.getScriptProperties();

const firstColumnDescriptors: Array<ColumnDescriptor> = [
	new ColumnDescriptor(
		"Nom",
		(student: Student) => student.firstName + " " + student.lastName,
		110.0, 
		nameColumnAttributes
	),
	new ColumnDescriptor("Équipe", (student: Student) => student.team, 35.28)
];

// Update the paper attendance sheets
export function updateAttendanceSheets() {
	const startTime = Date.now();

	const lastColumnDescriptors: Array<ColumnDescriptor> = [
		new ColumnDescriptor("Heure d’arrivée", emptyStringFunc, 45),
		new ColumnDescriptor("Signature", emptyStringFunc, 72),
		new ColumnDescriptor("Nom de l’accompagnant", emptyStringFunc, 72),
		new ColumnDescriptor("Heure de départ", emptyStringFunc, 45),
		new ColumnDescriptor("Signature", emptyStringFunc, 72),
		new ColumnDescriptor("Nom de l’accompagnant", emptyStringFunc, 72),
		new ColumnDescriptor("Devoirs", emptyStringFunc, 36.72),
		new ColumnDescriptor("Devoirs +", emptyStringFunc, 43.848),
		new ColumnDescriptor("Points sup", emptyStringFunc, 35.856),
		new ColumnDescriptor("Form", (student: Student) => student.forms, 72)
	];

	const morningColumnDescriptors: Array<
		ColumnDescriptor
	> = firstColumnDescriptors
		.concat([
			new ColumnDescriptor(
				"Après-midi",
				(student: Student) =>
					student.afternoonClass === null ? "" : student.afternoonClass.code,
				39.744
			)
		])
		.concat(lastColumnDescriptors);

	const afternoonColumnDescriptors = firstColumnDescriptors.concat(
		lastColumnDescriptors
	);

	Logger.log(afternoonColumnDescriptors);
	
	let i = Number(properties.getProperty("updateAttendanceSheets"));

	// Load sign-in sheet collection if it already exists, and create sign-in collection sheet if it doesn't
	let signInSheetBody = findOrCreateFileFromMyFrenchClassesTemplate(
		"Sign‐in sheets",
		"0B9GDqtqtKqfQd21iM3lIdHZvenM",
		true,
		i === 0
	);

	const ui = SpreadsheetApp.getUi();

	// Main program loop
	for (const classesAndProgramsLength = classesAndPrograms.length; i < classesAndProgramsLength; i++) {
		const classOrProgram = classesAndPrograms[i];
		appendSheetToDoc(
			signInSheetBody,
			classOrProgram.name + " — " + classOrProgram.teacher,
			"Feuille de présence — " + fullFrenchDate,
			classOrProgram.students,
			classOrProgram.isMorning
				? morningColumnDescriptors
				: afternoonColumnDescriptors
		)

		if (Date.now() - startTime > 330_000) {
			properties.setProperty("updateAttendanceSheets", String(i + 1));
			ui.alert("Wasn't able to update all attendance sheets in time. Please re-run to finish.");
			return;
		}
	}

	properties.setProperty("updateAttendanceSheets", "0")
	ui.alert("Successfully updated all attendance sheets.");
}

// Update the electronic attendance forms
export function updateAttendanceForms() {
	//Logger.log('starting loop');
	// Main program loop
	for (const classOrProgram of classesAndPrograms) {
		// Set some variables
		let code = classOrProgram.code;
		//Logger.log(code);
		let name = classOrProgram.name;
		let students: Array<Student> = classOrProgram.students;

		let nameList = [];
		// Get list of people in the class or program, and information about them
		for (const student of students) {
			nameList.push(student.firstName + " " + student.lastName);
		}

		// Prepare class attendance form

		// Load class attendance form if it already exists
		let file = getFileByName(code + " Attendance Form");
		let form: GoogleAppsScript.Forms.Form;
		if (file != null) {
			//Logger.log('  Updating form');
			form = FormApp.openById(file.getId());
			form.deleteAllResponses();
			let items = form.getItems();
			let attendanceGrid = items[0].asGridItem();
			if (!compareArrays(nameList, attendanceGrid.getRows())) {
				let pointsGrid = items[1].asCheckboxGridItem();
				attendanceGrid.setRows(nameList);
				pointsGrid.setRows(nameList);
			}
		} else {
			//Logger.log('Creating new form');
			// Create class attendance form if it doesn't
			form = FormApp.openById(
				DriveApp.getFileById("1OlgXTd6REvhecCLBUEuPnULVa19a4RmM1wjqQDis1gs")
					.makeCopy(
						code + " Attendance Form",
						DriveApp.getFolderById("0B9GDqtqtKqfQY3ZfV0szMm9oOWM")
					)
					.getId()
			);
			moveFile(form.getId(), "0B9GDqtqtKqfQNWdRMjd0WTl6M0U");
			let attendanceGrid = form.addGridItem();
			attendanceGrid.setTitle("Attendance");
			attendanceGrid.setColumns([
				"Present",
				"Came Late",
				"Left Early",
				"Absent"
			]);
			attendanceGrid.setRows(nameList);
			let pointsGrid = form.addCheckboxGridItem();
			pointsGrid.setTitle("Points");
			pointsGrid.setColumns([
				"Homework",
				"Bonus Homework",
				"Colors",
				"Extra Points"
			]);
			pointsGrid.setRows(nameList);
			let responseSpreadsheet = SpreadsheetApp.create(
				code + " Attendance Form Responses"
			);
			moveFile(responseSpreadsheet.getId(), "0B9GDqtqtKqfQbmxCd0xiMU1LWkE");
			form.setDestination(
				FormApp.DestinationType.SPREADSHEET,
				responseSpreadsheet.getId()
			);
		}
		form.setTitle(name + " Attendance Form");
		//Logger.log('  Done!');
	}
}

// Removes some extra columns that can appear in the form response spreadsheets when class rosters change.
// Needs to be a separate function because of weird Google Apps Script behavior
export function fixFormSpreadsheets() {
	let attendanceFormResponses = DriveApp.getFolderById(
		"0B9GDqtqtKqfQbmxCd0xiMU1LWkE"
	).getFiles();
	while (attendanceFormResponses.hasNext()) {
		let responseSpreadsheet = SpreadsheetApp.open(
			attendanceFormResponses.next()
		);
		let responseWorksheet = responseSpreadsheet.getSheets()[0];
		let responseRange = responseWorksheet.getDataRange();
		let responseValues = responseRange.getValues();
		let newResponseValues = responseValues;
		let deletionArray = [false];
		// Iterate over each column
		responseValues[0].forEach(function(element, index) {
			let firstInstance = responseValues[0].indexOf(element);
			if (firstInstance !== index) {
				for (let row = 1; row < responseValues.length; row++) {
					if (responseValues[row][firstInstance]) {
						responseValues[row][index] = responseValues[row][firstInstance];
					}
				}
			}
			deletionArray[index] = false; // Mark current column for preservation
		});
		responseRange.setValues(newResponseValues); // Update the array
		let column = 1;
		// Delete old columns
		deletionArray.forEach(function(deleteColumn) {
			if (deleteColumn) {
				responseWorksheet.deleteColumn(column);
			} else {
				column++;
			}
		});
	}
}

export function updateSuppliesSheets() {
	let suppliesSheetBody = findOrCreateFileFromMyFrenchClassesTemplate(
		"Supplies sheets",
		"0B9GDqtqtKqfQcWttVVo4bFBkNnM"
	);
	//Logger.log('starting loop');
	// Main program loop
	for (const classOrProgram of classesAndPrograms) {
		const columnDescriptors = firstColumnDescriptors.concat(
			classOrProgram.supplies.map(
				(supplyName: string) => new ColumnDescriptor(supplyName, emptyStringFunc, null)
			)
		);
		// Prepare class sign-in sheet
		appendSheetToDoc(
			suppliesSheetBody,
			classOrProgram.name + " — " + classOrProgram.teacher,
			"Feuille de matériel",
			classOrProgram.students,
			columnDescriptors
		);
	}
}
