/*
my-french-classes-attendance attendance management software
Copyright (C) 2020  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

let MONTH_NAMES = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
];
let FRENCH_MONTH_NAMES = [
	"Janvier",
	"Février",
	"Mars",
	"Avril",
	"Mai",
	"Juin",
	"Juillet",
	"Août",
	"Septembre",
	"Octobre",
	"Novembre",
	"Décembre"
];
let saturday = nextDay(new Date(), 6);
let saturdayMonth = MONTH_NAMES[saturday.getMonth()];
let saturdayFrenchMonth = FRENCH_MONTH_NAMES[saturday.getMonth()];
export const fullDate =
	saturdayMonth + " " + saturday.getDate() + ", " + saturday.getFullYear();
export const fullFrenchDate =
	saturday.getDate() + " " + saturdayFrenchMonth + " " + saturday.getFullYear();

// Used to get the next coming Saturday
export function nextDay(day: Date, dayOfWeek: number): Date {
	let returnDate = new Date();
	returnDate.setDate(day.getDate() + ((dayOfWeek + (7 - day.getDay())) % 7));
	return returnDate;
}
