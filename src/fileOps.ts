/*
my-french-classes-attendance attendance management software
Copyright (C) 2020  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

import { fullDate } from "./dateOps";

export function moveFile(sourceFileId: string, targetFolderId: string): void {
	let file = DriveApp.getFileById(sourceFileId);
	file
		.getParents()
		.next()
		.removeFile(file);
	file.moveTo(DriveApp.getFolderById(targetFolderId));
}

export function getFileByName(
	name: string
): GoogleAppsScript.Drive.File | null {
	let files = DriveApp.getFilesByName(name);
	if (files.hasNext()) {
		return files.next();
	}
	return null;
}

function findOrCreateFileFromTemplate(name: string, templateId: string) {
	let file = getFileByName(name);
	if (file == null) {
		file = DriveApp.getFileById(templateId).makeCopy(name);
	}
	return file;
}

export function findOrCreateFileFromMyFrenchClassesTemplate(
	title: string,
	destination: string,
	isDated: boolean = true,
	clearBody: boolean = true
): GoogleAppsScript.Document.Body {
	const body = DocumentApp.openById(
		findOrCreateFileFromTemplate(
			(isDated ? fullDate + " " : "") + title,
			"1e248-Ev9Pma4dCnM3vGZvW0yg6bJZKwPIt08MJhxHBE"
		).getId()
	).getBody();
	if (clearBody) {
		body.clear();
	}
	return body;
}
