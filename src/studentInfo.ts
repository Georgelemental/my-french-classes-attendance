/*
my-french-classes-attendance attendance management software
Copyright (C) 2020  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

import { ClassOrProgram, Student } from "./classes";

///// Basic variable definitions /////
const nameSpreadsheet = SpreadsheetApp.getActiveSpreadsheet();
const nameRange = nameSpreadsheet.getSheetByName("Name List").getDataRange();
const nameLastRow = nameRange.getLastRow();
const nameValues = nameRange.getValues();
const classRange = nameSpreadsheet.getSheetByName("Class List").getDataRange();
const classLastRow = classRange.getLastRow();
const classValues = classRange.getValues() as Array<Array<string>>;
const programRange = nameSpreadsheet
	.getSheetByName("Program List")
	.getDataRange();
const programLastRow = programRange.getLastRow();
const programValues = programRange.getValues() as Array<Array<string>>;

const classOrProgramMap = new Map();

// Create a new array of classes and programs
for (let i = 1; i < classLastRow; i++) {
	if (classValues[i][0] !== "") {
		classOrProgramMap.set(
			classValues[i][0],
			new ClassOrProgram(
				classValues[i][0],
				classValues[i][1],
				classValues[i][2],
				true,
				classValues[i][0].slice(-1) == "A",
				classValues[i][5].split(", "),
				[]
			)
		);
	}
}

// Create a new array of programs
for (let i = 1; i < programLastRow; i++) {
	if (programValues[i][0] !== "") {
		//Logger.log(true);
		classOrProgramMap.set(
			programValues[i][0],
			new ClassOrProgram(
				programValues[i][0],
				programValues[i][1],
				programValues[i][2],
				false,
				programValues[i][0].slice(-1) == "A",
				programValues[i][5].split(", "),
				[]
			)
		);
	}
}

// Add students to map
for (let i = 1; i < nameLastRow; i++) {
	let frenchClass: ClassOrProgram = classOrProgramMap.get(
		nameValues[i][3].toString()
	);
	let program: ClassOrProgram =
		nameValues[i][4] == "CO"
			? null
			: classOrProgramMap.get(nameValues[i][4].toString());
	let student = new Student(
		nameValues[i][0].toString(),
		nameValues[i][1].toString(),
		nameValues[i][2].toString(),
		frenchClass,
		program,
		nameValues[i][5]?.toString() ?? ""
	);
	if (frenchClass != null) {
		frenchClass.students.push(student);
	}
	if (program != null) {
		program.students.push(student);
	}
}

const classesAndPrograms: Array<ClassOrProgram> = Array.from(
	classOrProgramMap.values()
).filter(
	(classOrProgram: ClassOrProgram) => classOrProgram.students.length > 0
);

export { classesAndPrograms };
