/*
my-french-classes-attendance attendance management software
Copyright (C) 2020  Jules R Bertholet

This file is part of my-french-classes-attendance.

my-french-classes-attendance is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

my-french-classes-attendance is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with my-french-classes-attendance.  If not, see <https://www.gnu.org/licenses/>.

*/

export function compareArrays(array: Array<any>, array2: Array<any>): boolean {
	// compare lengths - can save a lot of time
	if (array.length != array2.length) {
		return false;
	}
	for (let i = 0, arrayLength = array.length; i < arrayLength; i++) {
		// Check if we have nested arrays
		if (array[i] instanceof Array && array2[i] instanceof Array) {
			// recurse into the nested arrays
			return compareArrays(array[i], array2[i]);
		} else if (array[i] != array2[i]) {
			// Warning—two different object instances will never be equal: {x:20} != {x:20}
			return false;
		}
	}
	return true;
}
